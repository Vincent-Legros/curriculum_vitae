import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSuitcase, faGraduationCap } from "@fortawesome/free-solid-svg-icons";
import SideBarInfo from './components/SideBarInfo';
import Position from './components/Position';
import data from './data/resume_data';

function Education({institution, studyType, area, startDate, endDate, city}){
    return (
        <section className="blocks">
          <div className="details">
            <header>
              <span className="date"><span>{startDate}</span> - <span>{endDate}</span></span>
              <h3>{studyType} {area}</h3>
              <span className="position-title">{institution}</span>
              {city && <span className="location">{city}</span>}
            </header>
            { false && <div>Relationship of the number with the answer to life, the universe and everything</div> }
          </div>
        </section>
    );
}

function App() {
  const showProject = false;

  return (
    <body lang="fr">
      <section id="main">
        <header id="title">
          <h1>Vincent Legros</h1>
          <h2 className="subtitle">{data.basics.label}</h2>
        </header>
      <section className="main-block concise">
      <h2><FontAwesomeIcon icon={faSuitcase}/> Parcours Professionel</h2>
      {data.work.map(work =>
        <Position
          endDate={work.endDate}
          startDate={work.startDate}
          name={work.name}
          location={work.location}
          position={work.position}
          highlights={work.highlights}
         />
      )}
      </section>
      { showProject && (
        <section className="main-block">
            <h2><i className="fa fa-folder-open"></i> Selected Projects</h2>
        </section>
      )}
      <section className="main-block concise">
        <h2><FontAwesomeIcon icon={faGraduationCap}/> Education</h2>
        { data.education.map(education => (
            <Education
                startDate={education.startDate}
                endDate={education.endDate}
                institution={education.institution}
                area={education.area}
                studyType={education.studyType}
                />
        ))}
      </section>
    </section>
    <SideBarInfo/>
    <div className="noprint">
      <center>
        <div style={{height: '2em'}}>
          &nbsp;
          <br />
          <button onClick={() => window.print()}>Save as PDF</button>
        </div>
      </center>
    </div>
  </body>

  );
}

export default App;
