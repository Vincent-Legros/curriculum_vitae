const data = {
  "basics": {
    "name": "Vincent Legros",
    "label": "Développeur logiciel",
    "image": "",
    "email": "legros.vincent@courrier.uqam.ca",
    "phone": "(438) 503-0496",
    "summary": "Développeur diplômé de l’Université du Québec à Montréal. Possédant une solide expérience en programmation web. J’ai une forte aptitude à l’apprentissage et je cherche à découvrir des solutions créatives à des problèmes complexes. Adapte de la programmation fonctionnelle, découvrir des nouvelles approches de programmation est pour moi essentielle. Ces compétences me permettent de créer du code de qualité et concevoir des logiciels facilement maintenables.",
    "location": {
      "address": "1729 ave de Salaberry",
      "postalCode": "J3L 5V1",
      "city": "Chambly",
      "countryCode": "CA",
      "region": "Québec"
    },
    "profiles": [{
      "network": "Linkedin",
      "username": "Vincent-Legros",
      "url": "https://twitter.com/john"
    }]
  },
  "work": [
    {
        "name": "UQAM",
        "position": [
            {
                "title":"Analyste de l'informatique junior",
                "startDate": "Avril 2020",
                "endDate": "Présent",
            },
            {
                "title":"Analyste de l'informatique stagiaire",
                "startDate": "Mai 2019",
                "endDate": "Avril 2020",
            }
        ],
        "location": "Montréal",
        "url": "https://company.com",
        "startDate": "Avril 2020",
        "endDate": "Présent",
        "summary": "Description…",
        "highlights": [
            "Concevoir des solutions logicielles conformes aux spécifications techniques",
            "Programmer des API REST en JavaScript et NodeJs selon les principes SOLID",
            "Programmer des interfaces utilisateurs avec Angular et React",
            "Participer aux processus de validation et vérification des exigences",
            "Présenter les fonctionnalités développées aux parties prenantes",
            "Rédiger des tests d’intégration et des tests unitaires",
            "Participer aux revues de code",
        ]
    },
    {
        "name": "Statistique Canada",
        "location": "Ottawa",
        "position": [
            {
                "title": "Développeur Java stagiaire",
                "startDate": "Avril 2018",
                "endDate": "Décembre 2018",
            }
        ],
        "url": "https://company.com",
        "startDate": "Avril 2018",
        "endDate": "Décembre 2018",
        "summary": "Description…",
        "highlights": [
            "Programmer de nouvelles fonctionnalités en Java",
            "Interpréter les spécifications produites par les analystes",
            "Participer aux rencontres de planification et de priorisation des tâches de développement",
        ]
    },
    {
        "name": "Jeunesses Musicales Canada",
        "position": [
            {
                "title": "Agent d'acceuil",
                "startDate": "Janvier 2015",
                "endDate": "Décembre 2015",
            }
        ],
        "url": "https://company.com",
        "startDate": "Janvier 2015",
        "endDate": "Décembre 2015",
        "summary": "Description…",
        "highlights": [
            "Maintenir le système de billetterie",
            "Établir la facture et balancer la caisse",
            "Rédiger les rapports de billetterie",
            "Coordonner les bénévoles et les artistes lors des concerts",
        ]
    },
    {
        "name": "Musée Stewart",
        "position": [{
            "title": "Guide Animateur",
            "startDate": "Juin 2011",
            "endDate": "Mars 2014",
        }],
        "url": "https://company.com",
        "startDate": "Juin 2011",
        "endDate": "Mars 2014",
        "summary": "Description…",
        "highlights": [
        ]
    }
],
"volunteer": [{
    "organization": "Organization",
    "position": "Volunteer",
    "url": "https://organization.com/",
    "startDate": "2012-01-01",
    "endDate": "2013-01-01",
    "summary": "Description…",
    "highlights": [
      "Awarded 'Volunteer of the Month'"
    ]
}],
  "education": [
      {
        "institution": "UQAM",
        "url": "https://institution.com/",
        "area": "en génie logiciel",
        "studyType": "Baccalauréat",
        "startDate": "Automne 2016",
        "endDate": "Hiver 2020",
        "score": "3.86",
        "courses": []
      },
      {
        "institution": "UQAM",
        "url": "https://institution.com/",
        "area": "en technologie d'affaire",
        "studyType": "Certificat",
        "startDate": "Hiver 2015",
        "endDate": "Automne 2017",
        "score": "3.86",
        "courses": []
      },
      {
        "institution": "HEC Montréal",
        "url": "https://institution.com/",
        "area": "en gestion des organismes culturels",
        "studyType": "D.E.S.S",
        "startDate": "Automne 2013",
        "endDate": "Hiver 2014",
        "score": "3.86",
        "courses": []
      },
    ],
  "awards": [{
    "title": "Award",
    "date": "2014-11-01",
    "awarder": "Company",
    "summary": "There is no spoon."
  }],
  "certificates": [{
    "name": "Certificate",
    "date": "2021-11-07",
    "issuer": "Company",
    "url": "https://certificate.com"
  }],
  "publications": [{
    "name": "Publication",
    "publisher": "Company",
    "releaseDate": "2014-10-01",
    "url": "https://publication.com",
    "summary": "Description…"
  }],
  "skills": [
      {
        "name": "Langues",
        "level": "Master",
        "keywords": [
            'Français',
            'Anglais',
        ]
      },
      {
        "name": "Langages de programmation",
        "level": "Master",
        "keywords": [
            "JavaScript",
            "TypeScript",
            "HTML",
            "CSS",
            "Elixir",
            "Clojure",
            "Java",
            "C++",
            "C#",
            "SQL",
        ]
      },
      {
          "name": "Technologies et cadriciels",
          "level": "Master",
          "keywords": [
              "Node.js",
              "React.js",
              "Angular",
              "Phoenix",
              "GraphQL",
              "PostgreSQL",
              "MySQL",
          ]
      },
      {
        "name": "Outils de développment",
        "level": "Master",
        "keywords": [
            "Git",
            "Windows",
            "Linux/GNU",
            "Mac",
            "VsCode",
            "IntelliJ",
            "Visual studio",
            "Vim",
        ]
      },
  ],
  "languages": [{
    "language": "Français",
    "fluency": "Native speaker"
  },{"language": "Anglais"}],
  "interests": [{
    "name": "Wildlife",
    "keywords": [
      "Ferrets",
      "Unicorns"
    ]
  }],
  "references": [{
    "name": "Jane Doe",
    "reference": "Reference…"
  }],
  "projects": [{
    "name": "Project",
    "description": "Description…",
    "highlights": [
      "Won award at AIHacks 2016"
    ],
    "keywords": [
      "HTML"
    ],
    "startDate": "2019-01-01",
    "endDate": "2021-01-01",
    "url": "https://project.com/",
    "roles": [
      "Team Lead"
    ],
    "entity": "Entity",
    "type": "application"
  }]
}

export default data;