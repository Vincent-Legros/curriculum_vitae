function Project(){
    return (
        <section className="blocks">
          <div className="date"><span>2015</span><span>2016</span></div>
          <div className="decorator"></div>
          <div className="details">
            <header>
              <h3>Some Project 1</h3>
              <span className="place">Some workplace</span>
            </header>
            <div>
              <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                <li>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec mi ante. Etiam odio eros, placerat
                  eu metus id, gravida eleifend odio
                </li>
              </ul>
            </div>
          </div>
        </section>

        )
    }

export default Project;