import React from 'react';
import data from '../data/resume_data';
import "../App.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faPhoneAlt } from "@fortawesome/free-solid-svg-icons";


const Profile = () => (
    <div className="side-block" id="contact">
      <h1>Profil</h1>
      <p className="profil-text">
        {data.basics.summary}
      </p>
    </div>
)

const PersonalInfo = () => (
    <div className="side-block" id="contact">
      <h1>Informations personelles</h1>
        <li><FontAwesomeIcon icon={faEnvelope} className="contact-icon"/> {data.basics.email}</li>
        <li><FontAwesomeIcon icon={faPhoneAlt} className="contact-icon"/> {data.basics.phone}</li>
        { /** <li><FontAwesomeIcon icon={faGlobe} className="contact-icon"/> {data.basics.url}</li>
        <li><FontAwesomeIcon icon={faLink} className="contact-icon"/> linkedin.com/in/john</li> **/ }
    </div>
)

const SideBarInfo = (props) => {
  return (
    <aside id="sidebar">
      <Profile/>
      <div className="side-block skills">
        {data.skills.map(skill => (
            <>
            <h3>{skill.name}</h3>
              <p>{skill.keywords.join(", ")}</p>
            </>
        ))}
      </div>
      <PersonalInfo/>
    </aside>
  )
};

export default SideBarInfo;