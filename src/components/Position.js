import React from 'react';

function Position({name,highlights = [],position,location}) {
    return (
        <section className="blocks">
          <div className="details">
            <header>
              <h3>{name}, {location}</h3>
             { position.map(({title,startDate,endDate}) => (
                 <span>
                    <div className="position-title"><h3>{title}</h3></div>
                    <span className="date"><span>{startDate}<span> - </span>{endDate}</span></span>
                 </span>
             ))}
             </header>
            <div>
              <ul>
                {highlights.map((hightlight) => (<li>{hightlight}</li>))}
              </ul>
            </div>
          </div>
        </section>
    )
};
export default Position;
